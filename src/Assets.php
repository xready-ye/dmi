<?php


namespace xr\dmi;


use yii\web\View;

class Assets {
    public static array $bundles = [];

    public static function register(View $view) {
        foreach (self::$bundles as $bundle) {
            $bundle::register($view);
        }
    }

    public static function addBundle($bundle) {
        $class = '\xr\dmi\bundles\\'.$bundle;

        if (!in_array($class, self::$bundles, true)) {
            self::$bundles[] = $class;
        }
    }
}