<?php


namespace xr\dmi;

use xr\dmi\objects;
use yii\base\Model;
use yii\web\IdentityInterface;
use yii\web\User;

class Auth extends Model implements IdentityInterface {
    //Only for config auth

    public static function findIdentity($id) {
        $auth = self::instance();

        if ($id === $auth->getId()) {
            return $auth;
        }

        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
    }

    private objects\Auth $auth;
    private User $user;

    public function getId() {
        return $this->auth->id;
    }

    public function getAuthKey() {
        return md5($this->auth->username.$this->auth->password);
    }

    public function validateAuthKey($authKey) {
        return ($authKey === $this->getAuthKey());
    }

    public function init() {
        $this->auth = DMI::auth();
        $this->user = DMI::user();
    }

    public function login($username, $password) {

        if ($username === $this->auth->username && $password == $this->auth->password) {
            return $this->user->login($this);
        }

        return false;
    }

    public function logout() {
        return $this->user->logout();
    }

    public function isAuth() {
        return ($this->user->id !== null);
    }
}