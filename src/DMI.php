<?php
//TOOD: Separate Auth object for Auth and Auth for View

namespace xr\dmi;


use xr\dmi\objects;
use xr\library\Db;
use yii\base\Module;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Request;
use yii\web\User;

class DMI extends Module
{
    public static function DMI() : objects\DMI {
        $instance = self::getInstance();
        $instance->initDMI();
        return $instance->DMI;
    }

    public static function auth() : objects\Auth {
        $instance = self::getInstance();
        $instance->initAuth();
        return $instance->Auth;
    }

    public static function id() : string {
        return self::getInstance()->id;
    }

    public static function db() : Db {
        return self::getInstance()->db;
    }

    public static function url($params) : string {
        if (is_string($params)) {
            $params = '/'.self::id().'/'.$params;
        } elseif (is_array($params)) {
            $params[0] = '/'.self::id().'/'.$params[0];
        }

        return Url::to($params);
    }

    public static function request() : Request {
        return self::getInstance()->request;
    }

    public static function user() : User {
        return self::getInstance()->user;
    }

    public static function view() : objects\DMI|objects\Auth {
        Assets::addBundle('DMI');

        if (Auth::instance()->isAuth()) {
            Assets::addBundle('WebixPro');
            Assets::addBundle('View');

            $ret = self::DMI();
        } else {
            Assets::addBundle('WebixGPL');
            Assets::addBundle('Auth');

            $ret = self::Auth();

            $ret->username = '';
            $ret->password = '';
        }

        return $ret;
    }

    public $defaultRoute = 'dmi';
    public $layout = false;

    private $config;
    private ?objects\DMI $DMI = null;
    private ?objects\Auth $Auth = null;

    private string $configPath;

    public function init()
    {
        $this->setComponents(
            [
                'user' => [
                    'class' => 'yii\web\User',
                    'identityClass' => 'xr\dmi\Auth',
                    'enableAutoLogin' => true,
                ],
                'request' => [
                    'class' => 'yii\web\Request',
                    'parsers' => [
                        'application/json' => 'yii\web\JsonParser',
                    ]
                ],
                'db' => [
                    'class' => 'xr\library\Db'
                ]
            ]
        );

        $this->setAliases([
            '@dmi' => $this->basePath
        ]);

        $this->configPath = \Yii::$app->basePath.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'dmi.json';

        parent::init();
    }

    private function getConfig() {
        if ($this->config === null) {
            //TODO: try for wrong config + check config with exception
            if (file_exists($this->configPath)) {
                $configContent = file_get_contents($this->configPath);

                $this->config = Json::decode($configContent);
            } else {
                throw new Exception('No DMI config', Exception::NO_CONFIG);
            }
        }

        return $this->config;
    }

    private function initAuth() {
        if ($this->Auth === null) {
            $config = $this->getConfig();

            $config['auth']['class'] = 'xr\dmi\objects\Auth';
            $config['auth']['title'] = $config['title'];

            $this->Auth = \Yii::createObject($config['auth']);
        }
    }

    private function initDMI() {
        if ($this->DMI === null) {
            $config = $this->getConfig();

            unset($config['auth']);

            $config['class'] = 'xr\dmi\objects\DMI';

            $this->DMI = \Yii::createObject($config);
        }
    }
}