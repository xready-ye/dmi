class DMIAuth {
    authFormId = 'xre_ui_auth';

    url;

    constructor(viewId, config) {
        $$(viewId).addView({
            view: "form",
            id:   this.authFormId,
            elements:[{
                view:  "text",
                label: "Username",
                name:  "username",
                id: this.authFormId + '_username'
            }, {
                view:  "text",
                type:  "password",
                label: "Password",
                name:  "password",
                id: this.authFormId + '_password'
            }, {
                view:  "checkbox",
                label: "Remember",
                name:  "remember",
                id: this.authFormId + '_remember'
            }, {
                cols:[
                    {
                        view:  "button",
                        value: "Login",
                        type:  "form",
                        click: () => {
                            this.auth();
                        }
                    }, {
                        view:  "button",
                        value: "Cancel",
                        click: () => {
                            $$(this.authFormId).clear();
                        }
                    }
                ]
            }]
        });

        this.url = config.loginUrl;

        webix.UIManager.addHotKey("Enter", () => {
            this.auth();
        }, $$(this.authFormId + '_username'));

        webix.UIManager.addHotKey("Enter", () => {
            this.auth();
        }, $$(this.authFormId + '_password'));
    }

    auth() {
        let params = $$(this.authFormId).getValues()

        XRE.DMI.screen.lock();

        webix.ajax().headers({
            "Content-type":"application/json"
        }).post(
            this.url,
            //params,
            JSON.stringify(params),
            {
                success: (text, data) => {
                    let json = data.json();

                    if (json && json.success) {
                        location.reload();
                    } else {
                        XRE.DMI.screen.unlock();
                        webix.alert('Wrong auth');
                    }
                },
                error: (text, data, xhr) => {
                    XRE.DMI.screen.unlock();

                    XRE.DMI.error('Auth error', 'Error: ' + xhr.status + ' ' + xhr.statusText);
                }
            }
        );
    }

    checkForm() {
        return XRE.isset($$(this.authFormId));
    }

    removeForm() {
        $$(this.DMI.id).removeView(this.authFormId);
    }
}