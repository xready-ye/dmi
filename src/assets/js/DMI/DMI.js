XRE.DMI = {
    viewId: 'xre_ui',

    views: {

    },

    screen: {
        init: function(id) {
            this.id = id;
            webix.extend($$(this.id), webix.ProgressBar);
        },
        lock: function() {
            $$(this.id).disable();
            $$(this.id).showProgress({type: "icon"});
        },
        unlock: function() {
            $$(this.id).hideProgress();
            $$(this.id).enable();
        }
    },

    error: function (title, text) {
        webix.alert({
            width: $$(this.id).$width * .7,
            type: "alert-error",
            title: title,
            text: text
        });
    },

    init: function (config) {
        webix.ready(() => {
            webix.ui.fullScreen();
            webix.ui({
                rows: [{
                    height: 0,
                    id: this.viewId,
                    rows: []
                }, {
                    //height: 50,
                    autoheight: true,
                    template: '<div style="text-align: center;">Powered by <a href="http://xready.ru" target="_blank">XReady Engine</a> & <a href="http://webix.com" target="_blank">Webix</a> (<a href="#" onclick="XRE.DMI.refreshConfig(); return false;">Refresh config</a>)</div>'
                }]
            });

            this.screen.init(this.viewId);

            if (config.auth) {
                this.auth = new DMIAuth(this.viewId, config);
            } else {
                this.view = new DMIView(this.viewId, config);
            }
        });
    }
}