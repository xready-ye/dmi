class DMIView {
    type = 'tabs';
    menuIdPrefix = 'xre_ui_tab'

    types = {
        tabs: DMIMenuTabs
    }

    id;
    menu;

    menuIdByName = {};

    configByMenuId = {};
    viewByMenuId = {};

    prepareMenuId(config) {
        return [this.menuIdPrefix, config.module, config.name].join('_')
    }

    prepareSelected(views) {
        let first = this.prepareMenuId(views[0]);

        if (location.hash !== '') {
            var hash = location.hash.substr(1);

            if (hash in this.configByMenuId) {
                first = hash;
            }
        }

        return first;
    }

    constructor(id, config) {
        this.id = id;
        this.menu = new this.types[this.type](this);

        for (let i in config.views) {
            let item = config.views[i];

            let menuId = this.prepareMenuId(item);

            this.menuIdByName[item.name] = menuId;
            this.configByMenuId[menuId] = item;

            this.menu.add(menuId, item.title);
        }

        this.menu.selected = this.prepareSelected(config.views);
        this.menu.show();
    }

    showMenuContent(menuId) {
        if (!(menuId in this.viewByMenuId)) {
            location.hash = menuId;

            let config = this.configByMenuId[menuId];

            this.viewByMenuId[menuId] = new XRE.DMI.views[config.module](menuId, config);
        }
    }
}