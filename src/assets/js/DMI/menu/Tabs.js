class DMIMenuTabs {
    viewsId = 'xre_ui_views';
    tabsId =  'xre_ui_tabs';

    view;
    selected;
    showed = false;
    options = [];
    cells = [];

    constructor(view) {
        this.view = view;
    }

    add(id, title) {
        if (this.showed) {
            $$(this.viewsId).addView({
                id: id,
                rows: [],
                responsive: true
            });

            $$(this.tabsId).addOption(id, title);
        } else {
            this.options.push({
                id: id,
                value: title
            });

            this.cells.push({
                id: id,
                rows: [],
                responsive: true
            });
        }
    }

    show() {
        $$(this.view.id).addView({
            id: this.tabsId,
            view: "tabbar",
            multiview: true,
            options: this.options,
            value: this.selected,
            on: {
                onChange: (menuId) => {
                    this.view.showMenuContent(menuId);
                }
            }
        });

        $$(this.view.id).addView({
            id: this.viewsId,
            cells: this.cells
        });

        $$(this.selected).show();
        this.view.showMenuContent(this.selected);
    }

    select(id) {
        $$(this.tabsId).setValue(id);
    }
}