class DMILayout {
    constructor(parentId, config) {
        let id = parentId + '_layout';

        let layout = {
            id: id
        };

        let views = [];

        if (config.rows.length > 0) {
            layout.rows = [];

            views = config.rows;
        } else if (config.cols.length > 0) {

            layout.cols = [];

            views = config.cols;
        }

        $$(parentId).addView(layout);

        for (let view in views) {
            let config = views[view];

            new XRE.DMI.views[config.module](id, config);
        }
    }
}

XRE.DMI.views.layout = DMILayout;