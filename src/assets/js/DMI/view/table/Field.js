class DMITableField {
    table;
    config;

    constructor(table, config) {
        this.table = table;
        this.config = config;
    }

    isColumn() {
        return !this.config.hidden;
    }

    prepareColumn() {
        let column = {
            id: this.config.name,
            fillspace: true
        };

        column.header = this.config.title;

        return column;
    }
}