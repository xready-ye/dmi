class DMITable {
    idPrefix = 'xre_ui_table'

    id;
    parentId;

    fields = {};

    paging;
    pagerId;
    pageSize = 20;

    constructor(parentId, config) {
        console.log('TABLE CONSTRUCT', config);

        this.parentId = parentId;
        this.id = this.parentId + '_table';


        this.init(config);

        let gridParams = {
            id: this.id,
            view: 'datatable',
            editable: false,
            navigation: true,
            editaction: "custom",
            select: 'cell',
            columns: this.prepareColumns(config),
            footer: false,
            url: config.dataUrl.paging
        }

        this.preparePagerParams(gridParams);

        $$(this.parentId).addView(gridParams);

        this.preparePagerView();
    }

    init(config) {
        this.paging = config.paging;

        for (let name in config.fields) {
            this.fields[name] = new DMITableField(this, config.fields[name]);
        }
    }

    preparePagerParams(gridParams) {
        if (this.paging) {
            this.pagerId = this.id + '_pager';
            gridParams.datafetch = this.pageSize;
            gridParams.pager = this.pagerId;
        }

    }

    preparePagerView() {
        if (this.paging) {
            $$(this.parentId).addView({
                view: "pager",
                id: this.pagerId,
                size: this.pageSize,
                template: "Total rows: #count# {common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()} Page #page+1# of #limit#"
            });
        }
    }

    prepareColumns(config) {
        let columns = [];

        for (let i in this.fields) {
            let field = this.fields[i];

            if (field.isColumn()) {
                columns.push(field.prepareColumn());
            }
        }

        return columns;
    }
}

XRE.DMI.views.table = DMITable;