const XRE = {
    keyExists: function (object, key) {
        return typeof object[key] !== 'undefined';
    },

    isset: function (variable) {
        return typeof variable !== 'undefined';
    },

    ucfirst: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLocaleLowerCase();
    }
}