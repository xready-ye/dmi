<?php


namespace xr\dmi\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class Auth extends AssetBundle
{
    public $sourcePath = '@dmi/assets';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $js = [
        'js/DMI/Auth.js'
    ];

    public $css = [

    ];
}