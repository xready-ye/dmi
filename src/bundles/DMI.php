<?php


namespace xr\dmi\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class DMI extends AssetBundle
{
    public $sourcePath = '@dmi/assets';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $js = [
        'js/XRE.js',
        'js/DMI/DMI.js'
    ];

    public $css = [

    ];
}