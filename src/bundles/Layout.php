<?php


namespace xr\dmi\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class Layout extends AssetBundle
{
    public $sourcePath = '@dmi/assets/js/DMI/view/layout';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $js = [
        'Layout.js'
    ];

    public $css = [

    ];
}