<?php


namespace xr\dmi\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class Report extends AssetBundle
{
    public $sourcePath = '@dmi/assets/js/DMI/view/report';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $js = [
        'Report.js'
    ];

    public $css = [

    ];
}