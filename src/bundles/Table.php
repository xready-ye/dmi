<?php


namespace xr\dmi\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class Table extends AssetBundle
{
    public $sourcePath = '@dmi/assets/js/DMI/view/table';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $js = [
        'Table.js',
        'Field.js',
    ];

    public $css = [

    ];
}