<?php


namespace xr\dmi\bundles;


use yii\web;

class View extends web\AssetBundle
{
    public $sourcePath = '@dmi/assets';
    public $jsOptions = [
        'position' => web\View::POS_HEAD
    ];

    public $js = [
        'js/DMI/menu/Tabs.js',
        'js/DMI/View.js',
    ];

    public $css = [

    ];
}