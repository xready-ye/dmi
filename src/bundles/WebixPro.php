<?php


namespace xr\dmi\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class WebixPro extends AssetBundle
{
    public $sourcePath = '@dmi/assets/webix/8.1pro';
    public $jsOptions  = [ 'position' => View::POS_HEAD ];
    public $js         = [ 'webix.js' ];
    public $css        = [ 'webix.css' ];
}