<?php
//TODO: Data validtion for Auth and Table
//TODO: Remembeer for auth
//TODO: Common format for API (success, data, etc.)
//TODO: Auth check

namespace xr\dmi\controllers;


use xr\dmi\Auth;
use xr\dmi\DMI;
use yii\rest\Controller;

class ApiController extends Controller {
    public function init() {
        parent::init();

        $this->detachBehavior('authenticator');
        $this->detachBehavior('rateLimiter');
    }

    public function actionIndex() {
        $this->redirect(DMI::url('dmi'));
    }
}