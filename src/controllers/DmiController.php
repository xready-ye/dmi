<?php


namespace xr\dmi\controllers;


use xr\dmi\Auth;
use xr\dmi\DMI;
use yii\web\Controller;

class DmiController extends Controller {
    public function actionIndex()
    {
        return $this->render('index', [
            'DMI' => DMI::view()
        ]);
    }

    public function actionLogout() {
        Auth::instance()->logout();

        $this->redirect(DMI::url('dmi'));
    }
}