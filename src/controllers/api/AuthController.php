<?php


namespace xr\dmi\controllers\api;


use xr\dmi\Auth;
use xr\dmi\controllers\ApiController;
use xr\dmi\DMI;

class AuthController extends ApiController {
    public function actionLogin() {
        if (Auth::instance()->isAuth()) {
            Auth::instance()->logout();
        }

        $ret = [
            'success' => false
        ];

        $post = DMI::request()->post();

        if (!empty($post) && array_key_exists('username', $post) && array_key_exists('password', $post)) {
            $ret['success'] = Auth::instance()->login($post['username'], $post['password']);
        }

        //$ret['is_auth'] = Auth::instance()->isAuth();

        return $ret;
    }

    public function actionLogout() {
        return Auth::instance()->logout();
    }
}