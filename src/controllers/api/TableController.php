<?php
//TODO: Auth check

namespace xr\dmi\controllers\api;


use xr\dmi\controllers\ApiController;
use xr\dmi\DMI;
use xr\dmi\objects\Table;
use xr\library\Dump;

class TableController extends ApiController {
    public function actionGet() {
        DMI::DMI();

        $tableName = DMI::request()->get('table');

        $table = Table::getTable($tableName);

        $ret = [
            'success' => true,
            'totalCount' => $table->getCount(),
            'data' => $table->get()
        ];

        return $ret;
    }
}