<?php


namespace xr\dmi\objects;

use xr\dmi;
use xr\dmi\Assets;
use yii\base\BaseObject;

class Auth extends BaseObject {
    //TODO: Separate types: config, DB auth, already exists (in Yii app)
    public bool $auth = true;
    public string $id;
    public string $username;
    public string $type;
    public string $password;
    public string $title;
    public string $baseUrl;
    public string $loginUrl;

    public function init() {
        parent::init();

        $this->id = $this->username;
        $this->loginUrl = dmi\DMI::url('api/auth/login');
    }
}