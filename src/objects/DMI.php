<?php


namespace xr\dmi\objects;

use xr\dmi\Assets;
use yii\base\BaseObject;

class DMI extends BaseObject
{
    const DEFAULT_MODULE = 'table';

    public static function completeConfig(array &$config, string $class) {
        $config['class'] = 'xr\\dmi\\objects\\'.$class;

        $config = \Yii::createObject($config);
    }

    public static function completeViewConfig(array|string &$config) {
        if (is_string($config)) {
            $config = [
                'name'=> $config
            ];
        }

        if (!array_key_exists('module', $config)) {
            $config['module'] = self::DEFAULT_MODULE;
        }

        self::completeConfig($config, ucfirst($config['module']));
    }

    public bool $auth = false;
    public string $title = 'Direct Management Interface';
    public array $views;
    public array $exclude;
    public string $baseUrl;

    public function init() {
        parent::init();

        foreach ($this->views as &$view) {
            self::completeViewConfig($view);
        }
    }
}