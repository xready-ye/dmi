<?php


namespace xr\dmi\objects;

use xr\dmi\objects\View;
use xr\dmi\objects;
use xr\dmi;

class Layout extends View
{
    public $rows = [];
    public $cols = [];

    public function init() {
        parent::init();

        foreach ($this->rows as &$row) {
            objects\DMI::completeViewConfig($row);
        }

        foreach ($this->cols as &$col) {
            objects\DMI::completeViewConfig($col);
        }

        dmi\Assets::addBundle('Layout');
    }
}