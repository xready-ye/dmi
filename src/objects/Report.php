<?php


namespace xr\dmi\objects;


use xr\dmi\objects\Table;
use xr\dmi;

class Report extends Table
{
    public function init() {
        parent::init();

        dmi\Assets::addBundle('Report');
    }
}