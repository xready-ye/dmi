<?php
//Старое:
//TODO: Постраничка
//TODO: Коллекции и getCombo
//TODO: Фильтры (посмотреть Pro)
//TODO: Сортировка
//TODO: Сортировка view и полей
//TODO: customFields в fields
//TODO: preventRow (подумать может не в самой таблице?)
//TODO: Редактирование (можеет заменить формой вызываеемой по сочетанию клавиш)
//TODO: Кастомный комбик (посмотреть еще раз в webix)
//TODO: Итог (summ и вот это всё)
//TODO: Доп. кнопки строк
//TODO: Доп. кнопки таблиц
//TODO: Стандартные кнопки
//TODO: Shema в Grid - что этоо и зачем

//Новое:
//TODO: редактируемый ID, отдельный ID для webix  втч с составными primaryKey
//TODO: Мультивыбор в editor (ref_...), мультивыбор в виде тегов, добавление нового
//TODO: Группировка в таблицах
//TODO: Pro: Подтаблицы


namespace xr\dmi\objects;

use xr\dmi;
use xr\dmi\objects;
use xr\library\db;
use yii\helpers\VarDumper;

class Table extends View
{
    private static array $tables = [];

    public static function getTable($name) : ?self {
        if (array_key_exists($name, self::$tables)) {
            return self::$tables[$name];
        }

        return null;
    }

    public $fields = [];
    public $customFields;
    public $paging = true;
    public $showId = false;    //Show ID field in grid
    public $editableId = false;
    public $dataOrder; //SQL ORDER BY for grid
    public $dictField; //Dictionary field for show in comboboxes
    public $dictOrder; //SQL ORDER BY for comboboxes
    public $userField; //Field where located owner ID
    public $defaultOrder;
    public $dataUrl;

    private db\Table $table;

    public function init() {
        parent::init();

        $this->dataUrl = [
            'paging' => dmi\DMI::url([
                'api/table/get',
                'table' => $this->name
            ]),
            'all' => dmi\DMI::url([
                'api/table/getAll',
                'table' => $this->name
            ])
        ];

        $this->table = dmi\DMI::db()->table($this->name);

        //VarDumper::dump($this->table, 10, true); die;

        foreach ($this->fields as &$field) {
            if (is_string($field)) {
                $field = [
                    'title' => $field
                ];
            }
        }

        foreach ($this->table->columns as $columnName => $column) {
            if (!array_key_exists($columnName, $this->fields)) {
                $this->fields[$columnName] = [
                    'title' => $columnName
                ];
            }

            $this->fields[$columnName]['column'] = $column;
        }

        foreach ($this->table->foreignKeys as $foreignKey) {
            if (!array_key_exists('reference', $this->fields[$foreignKey['column']])) {
                $this->fields[$foreignKey['column']]['reference'] = [];
            }

            $this->fields[$foreignKey['column']]['reference']['table'] = $foreignKey['foreignTable'];
            $this->fields[$foreignKey['column']]['reference']['field'] = $foreignKey['foreignColumn'];
        }

        foreach ($this->table->primaryKeys as $primaryKey) {
            $this->fields[$primaryKey]['hidden'] = !$this->showId;
            $this->fields[$primaryKey]['editable'] = $this->editableId;
        }

        foreach ($this->fields as $name => &$field) {
            $field['name'] = $name;
            objects\DMI::completeConfig($field, 'Table\\Field');
        }

        self::$tables[$this->name] = $this;

        dmi\Assets::addBundle('Table');
    }

    public function get() {
        return $this->table->select('*');
    }

    public function getCount() {
        if ($this->table->primaryKey !== null) {
            $countExpr = $this->table->primaryKey;
        } else {
            $countExpr = '*';
        }

        $select = $this->table->select($countExpr);

        return $select->count($countExpr);
    }
}