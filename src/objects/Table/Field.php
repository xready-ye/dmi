<?php
namespace xr\dmi\objects\Table;

use xr\dmi\objects\DMI;
use yii\base\BaseObject;
use yii\db\ColumnSchema;

class Field extends BaseObject
{
    const EDITORS_BY_TYPE = [
        'integer' => 'text',
        'string'  => 'text',
        'date'    => 'date',
        'boolean' => 'checkbox'
    ];

    const DEFAULT_EDITOR = 'text';

    public $name;
    public $title;
    public $hidden = false;
    public $editable = true;
    public $history = false;
    public $sortable = false;
    public $filtrable = false;
    public $filterValue;
    public $filterByActive = false;
    public $group = false;
    public $width;
    public $editor;
    public $order;
    public $clearAfterAdd = true;
    public $focusAfterAdd = false;
    public $footer;
    public $showTotal;
    public $template;
    public $reference;
    public ColumnSchema $column;

    public function init() {
        parent::init();

        if ($this->hidden) {
            $this->editable = false;
        }

        if ($this->editable && $this->editor === null) {
            if (array_key_exists($this->column->type, self::EDITORS_BY_TYPE)) {
                $this->editor = self::EDITORS_BY_TYPE[$this->column->type];
            } else {
                $this->editor = self::DEFAULT_EDITOR;
            }
        }

        if ($this->reference !== null) {
            DMI::completeConfig($this->reference, 'Table\\Reference');
        }
    }
}