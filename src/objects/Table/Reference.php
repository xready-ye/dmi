<?php


namespace xr\dmi\objects\Table;


use yii\base\BaseObject;

class Reference extends BaseObject {
    public $table;
    public $field;
    public $displayField = 'name';
    public $activeId;
    public $activeField;
}