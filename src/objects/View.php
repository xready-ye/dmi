<?php


namespace xr\dmi\objects;


use yii\base\BaseObject;

abstract class View extends BaseObject
{
    public $name;
    public $title;
    public $module;
    public $order = 99999;
}