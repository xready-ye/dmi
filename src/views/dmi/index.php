<?php
/* @var $this \yii\web\View */
/* @var $content string */
/* @var $DMI DMI */

use yii\helpers\Html;
use xr\dmi\objects\DMI;
use xr\dmi\Assets;

Assets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($DMI->title) ?></title>
    <?php $this->head() ?>
    <!--link rel="stylesheet" href="//cdn.webix.com/edge/webix.css" type="text/css"-->
    <!--script src="//cdn.webix.com/edge/webix.js" type="text/javascript"></script-->
</head>
<body>
<?php $this->beginBody() ?>
<script>
    XRE.DMI.init(<?=json_encode($DMI)?>)
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
